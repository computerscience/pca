	.file	"spill.c"
# GNU C (Debian 4.7.2-5) version 4.7.2 (x86_64-linux-gnu)
#	compiled by GNU C version 4.7.2, GMP version 5.0.5, MPFR version 3.1.0-p10, MPC version 0.9
# GGC heuristics: --param ggc-min-expand=100 --param ggc-min-heapsize=131072
# options passed:  -imultilib 32 -imultiarch i386-linux-gnu spill.c -m32
# -mtune=generic -march=i586 -O3 -fverbose-asm
# options enabled:  -fasynchronous-unwind-tables -fauto-inc-dec
# -fbranch-count-reg -fcaller-saves -fcombine-stack-adjustments -fcommon
# -fcompare-elim -fcprop-registers -fcrossjumping -fcse-follow-jumps
# -fdebug-types-section -fdefer-pop -fdelete-null-pointer-checks
# -fdevirtualize -fdwarf2-cfi-asm -fearly-inlining
# -feliminate-unused-debug-types -fexpensive-optimizations
# -fforward-propagate -ffunction-cse -fgcse -fgcse-after-reload -fgcse-lm
# -fgnu-runtime -fguess-branch-probability -fident -fif-conversion
# -fif-conversion2 -findirect-inlining -finline -finline-atomics
# -finline-functions -finline-functions-called-once
# -finline-small-functions -fipa-cp -fipa-cp-clone -fipa-profile
# -fipa-pure-const -fipa-reference -fipa-sra -fira-share-save-slots
# -fira-share-spill-slots -fivopts -fkeep-static-consts
# -fleading-underscore -fmath-errno -fmerge-constants -fmerge-debug-strings
# -fmove-loop-invariants -fomit-frame-pointer -foptimize-register-move
# -foptimize-sibling-calls -foptimize-strlen -fpartial-inlining
# -fpcc-struct-return -fpeephole -fpeephole2 -fpredictive-commoning
# -fprefetch-loop-arrays -free -fregmove -freorder-blocks
# -freorder-functions -frerun-cse-after-loop
# -fsched-critical-path-heuristic -fsched-dep-count-heuristic
# -fsched-group-heuristic -fsched-interblock -fsched-last-insn-heuristic
# -fsched-rank-heuristic -fsched-spec -fsched-spec-insn-heuristic
# -fsched-stalled-insns-dep -fschedule-insns2 -fshow-column -fshrink-wrap
# -fsigned-zeros -fsplit-ivs-in-unroller -fsplit-wide-types
# -fstrict-aliasing -fstrict-overflow -fstrict-volatile-bitfields
# -fthread-jumps -ftoplevel-reorder -ftrapping-math -ftree-bit-ccp
# -ftree-builtin-call-dce -ftree-ccp -ftree-ch -ftree-copy-prop
# -ftree-copyrename -ftree-cselim -ftree-dce -ftree-dominator-opts
# -ftree-dse -ftree-forwprop -ftree-fre -ftree-loop-distribute-patterns
# -ftree-loop-if-convert -ftree-loop-im -ftree-loop-ivcanon
# -ftree-loop-optimize -ftree-parallelize-loops= -ftree-phiprop -ftree-pre
# -ftree-pta -ftree-reassoc -ftree-scev-cprop -ftree-sink
# -ftree-slp-vectorize -ftree-sra -ftree-switch-conversion
# -ftree-tail-merge -ftree-ter -ftree-vect-loop-version -ftree-vectorize
# -ftree-vrp -funit-at-a-time -funswitch-loops -funwind-tables
# -fvect-cost-model -fverbose-asm -fzero-initialized-in-bss -m32 -m80387
# -m96bit-long-double -maccumulate-outgoing-args -malign-stringops
# -mfancy-math-387 -mfp-ret-in-387 -mglibc -mieee-fp -mno-red-zone
# -mno-sse4 -mpush-args -msahf -mtls-direct-seg-refs

	.text
	.p2align 4,,15
	.globl	var_prod_ele
	.type	var_prod_ele, @function
var_prod_ele:
.LFB0:
	.cfi_startproc
	pushl	%edi	#
	.cfi_def_cfa_offset 8
	.cfi_offset 7, -8
	pushl	%esi	#
	.cfi_def_cfa_offset 12
	.cfi_offset 6, -12
	pushl	%ebx	#
	.cfi_def_cfa_offset 16
	.cfi_offset 3, -16
	movl	16(%esp), %esi	# n, n
	movl	28(%esp), %edx	# i, tmp89
	movl	20(%esp), %eax	# A,
	movl	32(%esp), %ebx	# k, B
	movl	24(%esp), %ecx	# B,
	imull	%esi, %edx	# n, tmp89
	sall	$2, %esi	#, D.1375
	leal	(%ecx,%ebx,4), %ebx	#, B
	leal	(%eax,%edx,4), %edx	#, A
	xorl	%eax, %eax	# result
	leal	(%edx,%esi), %edi	#, end
	cmpl	%edi, %edx	# end, A
	jae	.L2	#,
	.p2align 4,,7
	.p2align 3
.L3:
	movl	(%edx), %ecx	# MEM[base: A_34, offset: 0B], tmp92
	addl	$4, %edx	#, A
	imull	(%ebx), %ecx	# MEM[base: B_35, offset: 0B], tmp92
	addl	%esi, %ebx	# D.1375, B
	addl	%ecx, %eax	# tmp92, result
	cmpl	%edx, %edi	# A, end
	ja	.L3	#,
.L2:
	popl	%ebx	#
	.cfi_restore 3
	.cfi_def_cfa_offset 12
	popl	%esi	#
	.cfi_restore 6
	.cfi_def_cfa_offset 8
	popl	%edi	#
	.cfi_restore 7
	.cfi_def_cfa_offset 4
	ret
	.cfi_endproc
.LFE0:
	.size	var_prod_ele, .-var_prod_ele
	.ident	"GCC: (Debian 4.7.2-5) 4.7.2"
	.section	.note.GNU-stack,"",@progbits
