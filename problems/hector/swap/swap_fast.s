	.file	"swap_fast.c"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%s\n"
	.text
	.p2align 4,,15
	.globl	panic
	.type	panic, @function
panic:
.LFB18:
	.cfi_startproc
	subl	$28, %esp
	.cfi_def_cfa_offset 32
	call	__errno_location
	movl	(%eax), %eax
	testl	%eax, %eax
	movl	32(%esp), %eax
	je	.L2
	movl	%eax, (%esp)
	call	perror
.L3:
	movl	$1, (%esp)
	call	exit
.L2:
	movl	%eax, 8(%esp)
	movl	stderr, %eax
	movl	$.LC0, 4(%esp)
	movl	%eax, (%esp)
	call	fprintf
	jmp	.L3
	.cfi_endproc
.LFE18:
	.size	panic, .-panic
	.section	.rodata.str1.1
.LC1:
	.string	"write"
	.section	.text.startup,"ax",@progbits
	.p2align 4,,15
	.globl	main
	.type	main, @function
main:
.LFB19:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	pushl	%edi
	pushl	%esi
	pushl	%ebx
	andl	$-16, %esp
	subl	$1056, %esp
	.cfi_offset 7, -12
	.cfi_offset 6, -16
	.cfi_offset 3, -20
	leal	32(%esp), %eax
	movl	$-4, 24(%esp)
	subl	%eax, 24(%esp)
	movl	%eax, 28(%esp)
	.p2align 4,,7
	.p2align 3
.L7:
	leal	32(%esp), %eax
	movl	$1024, 8(%esp)
	movl	%eax, 4(%esp)
	movl	$0, (%esp)
	call	read
	testl	%eax, %eax
	jle	.L21
	leal	32(%esp), %edi
	addl	%eax, %edi
	leal	-3(%edi), %esi
	cmpl	28(%esp), %esi
	jbe	.L15
	leal	32(%esp), %ecx
	.p2align 4,,7
	.p2align 3
.L9:
	movl	(%ecx), %ebx
	movl	%ebx, %edx
	shrl	$8, %edx
	sall	$8, %ebx
	andl	$16711935, %edx
	andl	$-16711936, %ebx
	orl	%ebx, %edx
	movl	%edx, (%ecx)
	addl	$4, %ecx
	cmpl	%ecx, %esi
	ja	.L9
	movl	24(%esp), %edx
	addl	%edi, %edx
	shrl	$2, %edx
	leal	36(%esp,%edx,4), %edx
.L8:
	subl	$1, %edi
	cmpl	%edi, %edx
	jae	.L13
	.p2align 4,,7
	.p2align 3
.L16:
	rolw	$8, (%edx)
	addl	$2, %edx
	cmpl	%edx, %edi
	ja	.L16
.L13:
	movl	%eax, 8(%esp)
	leal	32(%esp), %eax
	movl	%eax, 4(%esp)
	movl	$1, (%esp)
	call	write
	testl	%eax, %eax
	jns	.L7
	movl	$.LC1, (%esp)
	call	panic
	.p2align 4,,7
	.p2align 3
.L15:
	leal	32(%esp), %edx
	jmp	.L8
.L21:
	leal	-12(%ebp), %esp
	xorl	%eax, %eax
	popl	%ebx
	.cfi_restore 3
	popl	%esi
	.cfi_restore 6
	popl	%edi
	.cfi_restore 7
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
.LFE19:
	.size	main, .-main
	.ident	"GCC: (Debian 4.7.2-5) 4.7.2"
	.section	.note.GNU-stack,"",@progbits
