#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>

#define BUFFER_SIZE 1024

void panic(char *miss)
{
        if (errno != 0) perror(miss);
        else fprintf(stderr, "%s\n", miss);

        exit(1);
}

int main(int argc, char *argv[])
{
    char buffer[BUFFER_SIZE];
    char temp;
    int n;
    int i;

    while ((n=read(0, &buffer, BUFFER_SIZE)) > 0)
    {
        if(n < 0)
            panic("read");

        for(i = 1; i < n; i+=2)
        {
            temp = buffer[i];
            buffer[i] = buffer[i-1];
            buffer[i-1] = temp;
        }

        if(write(1, &buffer, n) < 0)
            panic("write");
    }

    return 0;
}
