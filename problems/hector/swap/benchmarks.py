from fast.benchmarks import benchmark, BenchmarkBase

@benchmark
class DataSizeBenchmark(BenchmarkBase):
    target = 'swap.3'
    instances =  20
    executions = 3
    xlabel = "Data size (Mbytes)"

    def input(self, instance):
        return "abc" * int((self.label(instance) * 1024 * 1024) / 3)

    def label(self, instance):
        return instance


@benchmark
class BigDataBenchmark(BenchmarkBase):
	target = 'swap.3'
	instances = 20
	executions = 3
	xlabel = "Data size (Mbytes)"

	def input(self, instance):
		return "abc" * int((self.label(instance) * 1024 * 1024) / 3)

	def label(self, instance):
		return instance * 10
