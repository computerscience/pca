#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <stdint.h>

#define BUFFER_SIZE 1024

void panic(char *miss)
{
        if (errno != 0) perror(miss);
        else fprintf(stderr, "%s\n", miss);

        exit(1);
}

int main(int argc, char *argv[]) {
    union {
        char* as_1;
        uint16_t* as_16;
        uint32_t* as_32;
    } ptr;

    char buffer[BUFFER_SIZE];
    char *end;
    char temp;
    int n;

    while ((n=read(0, &buffer, BUFFER_SIZE)) > 0) {
        if(n < 0)
            panic("read");

        ptr.as_1 = buffer;
        end = ptr.as_1 + n;

	// Accessing the buffer 4 bytes at a time to exploit
	// memory bandwith
        while(ptr.as_1 < (end - 3)) {
            *ptr.as_32 = ((*ptr.as_32 << 8) & 0xFF00FF00) |
                ((*ptr.as_32 >> 8) & 0x00FF00FF);

            ptr.as_32 += 1;
        }

        while(ptr.as_1 < (end - 1)) {
            *ptr.as_16 = ((*ptr.as_16 << 8) & 0xFF00) |
                ((*ptr.as_16 >> 8) & 0x00FF);
            
            ptr.as_16 += 1;
        }

        if(write(1, &buffer, n) < 0)
            panic("write");
    }

    return 0;
}
