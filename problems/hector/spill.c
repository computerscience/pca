int var_prod_ele(int n, int *A, int *B, int i, int k)
{
	int result = 0;
	
	A = A + i*n;
	B = B + k;
	int *end = A + n;

	while(A < end)
	{
		result += (*A) * (*B);
		A += 1;
		B += n;
	}
	
	return result;
}
