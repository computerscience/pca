#include <stdlib.h>
#include <stdio.h>


int main(int argc, char* argv[]) {
    // Exhaustive test
    int i;
    for(i = 0; i < 128; i+=sizeof(long long)) {
        // We simulate an address returned by malloc
        int* A = (int*) i;

        // Find the address of the first element aligned to 16 bytes:
        // ((@A + 15) / 16) * 16
        int* pAux = (int*)((((unsigned long)A) + 15) & ~0x0F);

        // Print the result
        printf("A = %d, pAux = %d\n", A, pAux);
    }
}
