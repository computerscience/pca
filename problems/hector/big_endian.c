#include <stdio.h>


int is_big_endian(void)
{
	long long x = 1;
	char *y = (char*)&x;

	return *y != 1;
}

int main(void)
{
	printf("Is %s endian\n", is_big_endian() ? "big" : "little");
	return 0;
}
