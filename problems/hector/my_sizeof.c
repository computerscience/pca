#include <stdio.h>

#define MY_SIZEOF(_type) ((int) (((_type*) 0) + 1))

struct A {
    char a;
    int b;
    int c;
};

int main(int argc, char* argv[]) {
    printf("%d\n", MY_SIZEOF(struct A));

    return 0;
}
