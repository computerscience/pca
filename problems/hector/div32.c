#include <stdio.h>

int div32(int x)
{
    return (x + ((x >> 31) & 31)) >> 5;
}

int main(void)
{
    int x;
    scanf("%d", &x);

    printf("%d\n", div32(x));
    return 0;
}
