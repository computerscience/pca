#include <stdio.h>

unsigned int i;

int main(void)
{
	for(i = 0; i <= 0xFFFFFFFF; ++i)
	{
		float* f = (float*)&i;

		if(i != (int) (double) i)
			printf("%x %d %d %f\n", i,  i, (int)(double)i, (double)i);
	}

	return 0;
}
