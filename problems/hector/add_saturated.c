#include <stdio.h>
#include <stdint.h>

// In this problem, we suppose that the integers are represented in Ca2.

int INT_MIN, INT_MAX;

void init_limits(void)
{
    union {
        unsigned int a;
        int b;
    } u;

    // In Ca2, -1 is represented by 0xFF...FF
    u.b = -1;

    // The maximum integer value is 0x7F...FF
    INT_MAX = (u.a >> 1); // This is why we right shift -1 as unsigned integer

    // The minimum integer value is 0x80...00
    INT_MIN = ~INT_MAX;  // Which is obtained negating INT_MAX
}

int adds(int x, int y)
{
    // y > 0 and x > INT_MAX - y => x + y overflows
    // We use here the first condition (y > 0) as a mask to simulate &&
    int overflow = -(y > 0) & (x > INT_MAX - y);

    // y < 0 and x < INT_MIN - y => x + y underflows
    // Same as before
    int underflow = -(y < 0) & (x < INT_MIN - y);

    // The result is correct if no overflow or underflow happens
    int correct = 1 - (overflow | underflow);

    // To avoid jumps, we use, as before, conditions as masks to void the
    // results to overwrite each other
    return  -overflow & INT_MAX
        |   -underflow & INT_MIN
        |   -correct & x+y;
}

int main(void)
{
    // We can avoid this using limits.h, but it's interesting to calculate
    // the max/min values without assuming the number of available bits
    init_limits();
    printf("INT_MIN: %d\nINT_MAX: %d\n\n", INT_MIN, INT_MAX);

    int x, y;
    scanf("%d", &x);
    scanf("%d", &y);

    printf("%d\n", adds(x, y));
    return 0;
}
