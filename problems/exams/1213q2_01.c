#include <stdio.h>
#include <unistd.h>

#define BUFFER_SIZE 1024

int main(int argc, char *argv[])
{
  char buff[BUFFER_SIZE]; // compiler, please: align this to 16
  int n1;

  while(((n1=read(0, buff, BUFFER_SIZE)) > 0)) {
    char* p = buff;
    char* end = buff + n1;
    __m128i* ps;

    while(p < (end-15)) {
      ps = (__m128i*) p;
      *ps =  _mm_abs_epi8(*ps);
      p += 16;
    }

    while(p < end) {
      if(*p < 0) *p = -(*p);
      p++;
    }
  }

  write(1, buff, n1);
}
