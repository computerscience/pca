from fast.benchmarks import BenchmarkBase, benchmark


@benchmark
class SimpleBenchmark(BenchmarkBase):
    target = 'empleats.3'
    instances = 20
    executions = 2
    xlabel = "Execution"

    def input(self, instance):
        return ""
