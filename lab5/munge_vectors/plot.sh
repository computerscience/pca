for f in *.csv
do
    bits=${f%%.csv}
    gnuplot -e "set ylabel 'Cycles'; set xlabel 'Alignment';"`
               `"set term pdf color; set output '${bits}_simple.pdf';"`
	       `"set key below;"`
	       `"set xrange[0:64];"`
               `"plot '${f}' using 1:2 with lines title '${bits}';"

    gnuplot -e "set ylabel 'cycles/access'; set xlabel 'Alignment';"`
               `"set term pdf color; set output '${bits}_norm.pdf';"`
	       `"set key below;"`
	       `"set xrange[0:64];"`
               `"plot '${f}' using 1:3 with lines title '${bits}';"
done

gnuplot -e "set ylabel 'Cycles'; set xlabel 'Alignment';"`
           `"set term pdf color; set output 'all_simple.pdf';"`
	   `"set key below;"`
	   `"set xrange[0:64];"`
           `"plot '8.csv' using 1:2 with lines title '8',"`
           `"'16.csv' using 1:2 with lines title '16',"`
           `"'32.csv' using 1:2 with lines title '32',"`
           `"'64.csv' using 1:2 with lines title '64'"

gnuplot -e "set ylabel 'cycles/access'; set xlabel 'Alignment';"`
           `"set term pdf color; set output 'all_norm.pdf';"`
	   `"set key below; set yrange [0:3];"`
	   `"set xrange[0:64];"`
           `"plot '8.csv' using 1:3 with lines title '8',"`
           `"'16.csv' using 1:3 with lines title '16',"`
           `"'32.csv' using 1:3 with lines title '32',"`
           `"'64.csv' using 1:3 with lines title '64'"

f="8.csv"
bits=8
gnuplot -e "set ylabel 'cycles/access'; set xlabel 'Alignment';"`
               `"set term pdf color; set output '${bits}_norm.pdf';"`
	       `"set key below;"`
`"set yrange[1.5:1.6];"`
	       `"set xrange[0:64];"`
               `"plot '${f}' using 1:3 with lines title '${bits}';"
