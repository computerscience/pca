	.file	"rgb_spatial_access.c"
	.text
	.p2align 4,,15
	.globl	rgb_verd
	.type	rgb_verd, @function
rgb_verd:
.LFB18:
	.cfi_startproc
	movl	$buffer, %eax
	.p2align 4,,7
	.p2align 3
.L2:
	movl	$256, (%eax)
	movl	$16777217, 4(%eax)
	movl	$65536, 8(%eax)
	addl	$12, %eax
	cmpl	$buffer+300000000, %eax
	jb	.L2
	rep
	ret
	.cfi_endproc
.LFE18:
	.size	rgb_verd, .-rgb_verd
	.section	.text.startup,"ax",@progbits
	.p2align 4,,15
	.globl	main
	.type	main, @function
main:
.LFB19:
	.cfi_startproc
	movl	$buffer, %eax
	.p2align 4,,7
	.p2align 3
.L6:
	movl	$256, (%eax)
	movl	$16777217, 4(%eax)
	movl	$65536, 8(%eax)
	addl	$12, %eax
	cmpl	$buffer+300000000, %eax
	jb	.L6
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	andl	$-16, %esp
	subl	$16, %esp
	movl	$300000000, 8(%esp)
	movl	$buffer, 4(%esp)
	movl	$1, (%esp)
	call	write
	xorl	%eax, %eax
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
.LFE19:
	.size	main, .-main
	.comm	buffer,300000000,32
	.ident	"GCC: (Debian 4.7.2-5) 4.7.2"
	.section	.note.GNU-stack,"",@progbits
