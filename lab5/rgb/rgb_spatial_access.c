#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>


typedef struct {
   char r;
   char g;
   char b;
} Tpixel;

#define ROWS 5000
#define COLUMNS 20000

Tpixel buffer[ROWS][COLUMNS];

void rgb_verd()
{
  unsigned int i,j;

  union {
    Tpixel* as_pixel;
    uint32_t* as_32;
  } ptr;

  ptr.as_pixel = (Tpixel*) buffer;
  Tpixel* end = ptr.as_pixel + ROWS * COLUMNS;

  while(ptr.as_pixel < end)
  {
    *ptr.as_32     = 0x00000100;
    *(ptr.as_32+1) = 0x01000001;
    *(ptr.as_32+2) = 0x00010000;

    ptr.as_pixel += 4;
  }
}

int main(int argc, char *argv[])
{
  rgb_verd();

  write(1, buffer, sizeof(buffer));

  return(0);
}
