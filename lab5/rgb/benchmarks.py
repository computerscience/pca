from fast.benchmarks import benchmark, BenchmarkBase

@benchmark
class SpatialBenchmark(BenchmarkBase):
    target = 'rgb.3'
    candidates = ('rgb_spatial.3', 'rgb_spatial_access.3')
    instances = 20
    executions = 3
    xlabel = "Execution"

    def input(self, instance):
        return ""

@benchmark
class AccessBenchmark(SpatialBenchmark):
    target = 'rgb_spatial.3'
    candidates = ('rgb_spatial_access.3',)
