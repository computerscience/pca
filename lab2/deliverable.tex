\documentclass[a4paper,11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[usenames,dvipsnames]{color}
\usepackage{graphicx}
\usepackage[justification=centering,labelfont=bf]{caption}
\usepackage{listings}
\lstset{ % For diff
  basicstyle={\footnotesize\ttfamily},
  breakatwhitespace=false,
  breaklines=true,
  language=C,
  frame=single,
  captionpos=b,
  extendedchars=true,
  showspaces=false,
  showstringspaces=false,
  escapeinside=||,
  showtabs=false,
  tabsize=8,
  moredelim=**[is][\color{OliveGreen}]{@}{@},
}

\newcommand{\blueline}[1]{\textcolor{blue}{#1}}

\newcommand{\assignatura}{Programació Conscient de l'Arquitectura}
\newcommand{\titol}{First Deliverable}

\newcommand{\Pautor}{Héctor Ramón Jiménez}
\newcommand{\Sautor}{Alvaro Espuña Buxó}

\newcommand{\HRule}{\rule{\linewidth}{0.5mm}}

\newcounter{ProblemCounter}

\begin{document}
\begin{titlepage}
  \begin{center}
    \textsc{\Large \assignatura}\\[1.5cm]
    \HRule \\[0.4cm]
     { \huge \bfseries \titol \\[0.4cm] }
    \HRule \\[2.5cm]
    \begin{minipage}{0.4\textwidth}
      \begin{flushleft}
        \large \Pautor
      \end{flushleft}
    \end{minipage}
    \begin{minipage}{0.4\textwidth}
      \begin{flushright}
        \large \Sautor
      \end{flushright}
    \end{minipage}

    \vfill
    {\large \today}
  \end{center}
\end{titlepage}
\begin{enumerate}
  \setcounter{enumi}{3} % Next step will be 4
  \item Compile the \texttt{pi.c} program with \texttt{gcc}, O0 optimization level,
    \texttt{gprof} profiling option \texttt{-pg}, and the debug option
    (\texttt{-g}). Using \texttt{gprof}, answer the following questions:
    \begin{enumerate}
    \item Which is the most invoqued routine by the program? \\
      {\bf DIVIDE (exactly 30014 times)}
    \item Which is the most CPU time consuming routine? \\
      {\bf DIVIDE (totalling $\sim$50\% of the execution time)}
    \item Which is the most CPU time consuming source code line? \\
      {\bf Line 19, inside DIVIDE (\texttt{q = u / n;})}
    \item Does it appear the system mode execution time in \texttt{gprof} output? \\
      {\bf No, gprof doesn't show any information about OS code.}
    \end{enumerate}
    \vspace{2cm}
  \item Repeat the previous experiment compiling now with O3
    optimization level:
    \begin{enumerate}
      \item Which differences you can observe looking at the \emph{flat profile}
        (significant changes on the routine weights, routines that
        disappear/appear,...)? \\
        {\bf gprof only shows the \texttt{calculate} routine. There's no
          trace of \texttt{DIVIDE}, \texttt{SUBTRACT},...}
      \item Do you know why there are those differences? \\
        {\bf Observing the assembly code generated we can see that this
          happens because the compiler has performed function
          inlining. All the function calls have been replaced for the
          code of the function themselves in the \texttt{calculate}
          routine.  The other functions are still defined and \emph{callable}
          though, since the compiler doesn't know if \emph{somebody} will use them.}
    \end{enumerate}
    \newpage
   \setcounter{enumi}{6}
   \item The \texttt{pi\_times.c} program use the system call \texttt{times()} in order
     to show the execution time (decomposed in user mode and system
     mode) for each call to \texttt{calculate()}.
     \begin{enumerate}
       \item Observe the differences between \texttt{pi.c} and \texttt{pi\_times.c}
         programs and how the system call \texttt{times()} is called. \\
         % There must be a better way to do line-based highlighting
         % (minimizing dependencies) for a unified diff...
         \begin{lstlisting}
--- pi.c	2014-02-19 14:23:10.000000000 +0100
+++ pi_times.c	2014-02-19 14:25:07.000000000 +0100
|\blueline{@@ -1,6 +1,8 @@}|
 #include <memory.h>
 #include <stdio.h>
 #include <stdlib.h>
@+#include <sys/times.h>@
@+#include <unistd.h>@

 int N, N4;
 char a[25480], b[25480], c[25480];
|\blueline{@@ -122,6 +124,9 @@}|
 void calculate( void )
 {
     int j;
@+    struct tms start, end;@
@+@
@+    if (times(&start) == (clock_t)-1) exit(0);@

     N4 = N + 4;

|\blueline{@@ -156,6 +161,12 @@}|
     MULTIPLY( a, 4 );

     progress();
@+@
@+    if (times(&end) == (clock_t)-1) exit(0);@
@+    fprintf(stderr,@
@             "\n Timing amb crida al sistems times:"@
@   "user %f segons, system: %f segons\n",@
@             (float)(end.tms_utime-start.tms_utime)/@
@   sysconf(_SC_CLK_TCK),@
@             (float)(end.tms_stime-start.tms_stime)/@
@   sysconf(_SC_CLK_TCK));@
 }

 /*
         \end{lstlisting}
       \item Modify \texttt{pi\_times.c} program so that \texttt{getrusage()} system call
         is used instead of \texttt{times()} system call. \\
         {\bf See attached file \texttt{pi\_rusage.c}}

     \end{enumerate}
     \newpage
   \item Run the script with NMIN= 500, NMAX= 10000 and NSTEP= 500,
     and do the following figures:
     \vspace{1cm}
     \begin{enumerate}
       \item One figure that shows elapsed time (Y axis) function of
         the number of decimals computed (X axis) for the original
         \texttt{pi.c} compiled with \texttt{-O0}, \texttt{-O1}, and
         \texttt{-O3 march=native} compiler options.
         You can create a figure for each case or all cases in the same
         figure.

         \begin{figure}[h!]
           \includegraphics[width=1\textwidth]{pi_a.pdf}
           \caption{Figure showing elapsed time as function of the
             number of decimals places of pi calculated. Three different
             compiler levels of optimization were used.}
         \end{figure}

         \newpage
       \item Another figure that shows the $\frac{elapsed\:time}{number\:of\:
         decimals\;computed}$ (Y axis) function of the number of decimals
         computed (X axis) for the \texttt{pi.c} program for \texttt{-O0},
         \texttt{-O1}, and \texttt{-O3 -march=native} compiler
         options.
         You can create a figure for each case or all cases in the
         same figure.
         \begin{figure}[h!]
           \includegraphics[width=1\textwidth]{pi_b.pdf}
           \caption{Figure showing the linear behaviour of
             ${elapsed\:time}/{decimals}$. The
             linearity is independent of the optimization introduced
             by the compiler. }
         \end{figure}
       \item Explain the execution differences and the shape of the
         figures.
         {\bf We can see how the executable produced with -O3 flag
           runs faster than the O1, and the O1 faster than the O0 but
           only by a constant factor.

           \vspace{0.3cm}

           The graphs show that the execution time is quadratic on the number
           of computed decimals. In Figure 2, we can see the linear behaviour of
           $\frac{elapsed\:time}{number\:of\:decimals\;computed}$. The
           decimals are stored in the \texttt{a} array, so every arithmetic
           operation needs to traverse the \emph{significant part} of
           the array, hence the quadratic behaviour.}

     \end{enumerate}

\end{enumerate}
\end{document}
