#!/usr/bin/env python
import difflib
import os
import re
import sys
import subprocess


def compile(source, compiler='gcc', olevel=0, debug=False, profiling=False, native=False, out=None):
    cmd = [compiler, '-O%d' % olevel]

    if out is None:
        prog_name, ext = os.path.splitext(source)
        out = "%s_o%d.exe" % (prog_name, olevel)

    if debug:
        cmd.append('-g')
    if profiling:
        cmd.append('-pg')
    if native:
        cmd.append('-march=native')

    cmd.extend(['-o', out])
    cmd.append(source)

    print "Compiling %s... (%s)" % (source, ' '.join(cmd))
    if subprocess.call(cmd):
        if native:
            print "Warning: -march=native unsupported"
            return compile(source, compiler=compiler, debug=debug, profiling=profiling, native=False, out=out)
        else:
            raise RuntimeError("Error when compiling: %s" % source)

    return out


def compile_with_levels(source):
    return [
        compile(source, olevel=0),
        compile(source, olevel=1),
        compile(source, olevel=3, native=True)
    ]


def check_diffs(exec1, exec2, NMIN, NMAX, NSTEP, NEXEC):
    for N in xrange(NMIN, NMAX+1, NSTEP):
        print "Testing N=%d..." % N

        out1 = output_execution(exec1, N)
        out2 = output_execution(exec2, N)

        differences = ''.join(difflib.unified_diff(out1, out2))

        if differences:
            print "Differences detected with N=%d" % N
            print differences
            sys.exit(1)


def output_execution(exe, *args):
    cmd = ['./%s' % exe]
    cmd.extend(map(str, args))

    process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, _ = process.communicate()
    return out


def timed_execution(exe, *args):
    cmd = ['time', '-f', 'fast|%e|fast', "./%s" % exe]
    cmd.extend(map(str, args))

    process = subprocess.Popen(cmd, stdout=open("/dev/null"), stderr=subprocess.PIPE)
    out, err = process.communicate()

    if process.returncode:
        raise RuntimeError("Error when executing %s with args: %s" % (exe, args))

    elapsed = float(re.search(r'fast\|(\d+\.\d+)\|fast', err).group(1))
    return elapsed


def stats(exe, NEXEC, NMIN, NMAX, NSTEP, statsfile):
    for decimals in xrange(NMIN, NMAX+1, NSTEP):
        times = []
        for _ in xrange(NEXEC):
            times.append(timed_execution(exe, decimals))

        row = "%d %.4f\n" % (decimals, sum(times)/NEXEC)
        statsfile.write(row)
        sys.stdout.write(row)


def gnuplot(gnuplot_cmd):
    cmd = ['gnuplot', '-e', ';'.join(gnuplot_cmd)]
    subprocess.call(cmd)


def generate_graphs(prog_name, files):
    # First graph: Time - Decimals
    gnuplot([
        'set ylabel "Time (s)"',
        'set xlabel "Decimals"',
        "set term pdf color",
        'set output "%s_a.pdf"' % prog_name,
        "plot %s" % ', '.join('"{0}"'.format(name) for name in files)
    ])

    # Second graph
    gnuplot([
        'set ylabel "Time (s) / Decimals"',
        'set xlabel "Decimals"',
        "set term pdf color",
        'set output "%s_b.pdf"' % prog_name,
        "plot %s" % ', '.join('"{0}" using 1:($2/$1) title "{1}"'.format(name, name) for name in files)
    ])


if __name__ == "__main__":
    try:
        original, optimized = sys.argv[1:3]
        NMIN, NMAX, NSTEP, NEXEC = map(int, sys.argv[3:])
    except ValueError:
        print "error: too few arguments"
        print "Ex: benchmark.py pi.c pi.c 500 10000 500 3"
        sys.exit(1)

    originals = compile_with_levels(original)
    optimizeds = compile_with_levels(optimized)
    stat_files = []

    for orig, opti in zip(originals, optimizeds):
        name, _ = os.path.splitext(opti)

	if orig != opti:
            print "Checking differences between %s and %s..." % (orig, opti)
            check_diffs(orig, opti, NMIN, NMAX, NSTEP, NEXEC)

        with open("%s.stats" % name, 'w') as f:
            print "Generating stats %s..." % f.name
            stats(opti, NEXEC, NMIN, NMAX, NSTEP, statsfile=f)
            stat_files.append(f.name)

    print "Generating graphs..."
    prog_name, _ = os.path.splitext(optimized)
    generate_graphs(prog_name, stat_files)
