#include <memory.h>
#include <stdio.h>
#include <stdlib.h>

int N, N4;
char a[10240], b[10240], c[10240];
char string[100];

#define D239_ITER(_k) {				\
   index = ((unsigned) r) * 10 + (unsigned)x[(_k)];	\
   x[(_k)] = table239q[index];				\
   r = table239r[index]; }

#define D25_ITER(_k) {				\
   index = ((unsigned) r) * 10 + (unsigned)x[(_k)];	\
   x[(_k)] = table25q[index];				\
   r = table25r[index]; }

#define DIV_ITER(_k) {				\
    u = r * 10 + x[(_k)];			\
    q = u / n;					\
    r = u % n; x[(_k)] = q; }

#define MUL_ITER(_k) {				\
    q = n * x[(_k)] + r;			\
    r = q / 10;					\
    x[(_k)] = q - r * 10; }

#define SUB_ITER(_k) {				\
    aux = y[(_k)] - z[(_k)];			\
    x[(_k)] = aux + ((aux >> 7) & 10);		\
    z[(_k)-1] = z[(_k)-1] + ((aux >> 7) & 1); }


char buffer[BUFSIZ];


typedef unsigned char QR;

QR table239q[2390];
QR table239r[2390];
QR table25q[250];
QR table25r[250];
QR qr;


QR table5[10] = {0,1,2,3,4,0,1,2,3,4};

void init_table239(void) {
  int i;
  QR j, k;
  for (j=0; j < 239; ++j) {
    for (i=0, k=0; i < 2390; i+=239, k++){
      table239q[i+j] = k;
      table239r[i+j] = j;
    }
  }
}

void init_table25(void) {
  int i;
  QR j, k;
  for (j=0; j < 25; ++j) {
    for (i=0, k=0; i < 250; i+=25, k++){
      table25q[i+j] = k;
      table25r[i+j] = j;
    }
  }
}

__attribute__((always_inline))
inline void DIVIDE_239( char *x)
{
   unsigned index, k;
   QR r;
    r = 0;
    for( k = 0; k <= N4 - 11; k+=12 )
    {
	D239_ITER(k);
	D239_ITER(k+1);
	D239_ITER(k+2);
	D239_ITER(k+3);
	D239_ITER(k+4);
	D239_ITER(k+5);
	D239_ITER(k+6);
	D239_ITER(k+7);
	D239_ITER(k+8);
	D239_ITER(k+9);
	D239_ITER(k+10);
	D239_ITER(k+11);
    }
    for(; k <= N4; k++ ) {
	D239_ITER(k);
    }
}

__attribute__((always_inline))
inline void DIVIDE_25( char *x)
{
    unsigned index, k;
    QR r;
    r = 0;
    for( k = 0; k <= N4 - 11; k+=12 )
    {
	D25_ITER(k);
	D25_ITER(k+1);
	D25_ITER(k+2);
	D25_ITER(k+3);
	D25_ITER(k+4);
	D25_ITER(k+5);
	D25_ITER(k+6);
	D25_ITER(k+7);
	D25_ITER(k+8);
	D25_ITER(k+9);
	D25_ITER(k+10);
	D25_ITER(k+11);
    }
    for(; k <= N4; k++ ) {
	D25_ITER(k);
    }
}

void DIVIDE_5( char *x)
{
    int j, k;
    unsigned int q, r, u;
    long v;

    r = 0;
    for( k = 0; k <= N4; k++ )
    {
      //      printf("%d %d\n", r , x[k]);
      q = r*2;
      r = table5[x[k]];
      // Test if x[k] is greater than 4
      q += ((x[k] & 8) >> 3) | ((x[k] & 4) && (x[k] & 3));
      x[k] = q;
    }
}


void LONGDIV( char *x, int n )
{
    int j, k;
    unsigned q, r, u;
    long v;

    r = 0;
    for( k = 0; k <= N4 - 11; k+=12 )
      {
	DIV_ITER(k);
	DIV_ITER(k+1);
	DIV_ITER(k+2);
	DIV_ITER(k+3);
	DIV_ITER(k+4);
	DIV_ITER(k+5);
	DIV_ITER(k+6);
	DIV_ITER(k+7);
	DIV_ITER(k+8);
	DIV_ITER(k+9);
	DIV_ITER(k+10);
	DIV_ITER(k+11);
      }
    for (; k <= N4; k++) {
	u = r * 10 + x[k];
	q = u / n;
	r = u - q * n;
	x[k] = q;
    }

}

void MULTIPLY( char *x, int n )
{
    int j, k;
    unsigned q, r, u;
    long v;
    r = 0;
    for( k = N4; k >= 11; k-=12 )
    {
      MUL_ITER(k);
      MUL_ITER(k-1);
      MUL_ITER(k-2);
      MUL_ITER(k-3);
      MUL_ITER(k-4);
      MUL_ITER(k-5);
      MUL_ITER(k-6);
      MUL_ITER(k-7);
      MUL_ITER(k-8);
      MUL_ITER(k-9);
      MUL_ITER(k-10);
      MUL_ITER(k-11);
    }
    for (; k >= 0; k--) {
        q = n * x[k] + r;
        r = q / 10;
        x[k] = q - r * 10;
    }

}

void SET( char *x, int n )
{
    memset( x, 0, N4 + 1 );
    x[0] = n;
}


__attribute__((always_inline))
inline void SUBTRACT( char *x, char *y, char *z )
{
    int j, k;
    unsigned q, r, u;
    char aux, mask;
    long v;

    for( k = N4; k >= 11; k-=12 )
    {
      SUB_ITER(k);
      SUB_ITER(k-1);
      SUB_ITER(k-2);
      SUB_ITER(k-3);
      SUB_ITER(k-4);
      SUB_ITER(k-5);
      SUB_ITER(k-6);
      SUB_ITER(k-7);
      SUB_ITER(k-8);
      SUB_ITER(k-9);
      SUB_ITER(k-10);
      SUB_ITER(k-11);
    }
    for (; k>= 0; k--) {
      SUB_ITER(k);
    }

}


void calculate( void );
void progress( void );
void epilog( void );


int main( int argc, char *argv[] )
{
    N = 10000;
    if (argc>1) N = atoi(argv[1]);
    setbuf(stdout, buffer);

    init_table239();
    init_table25();

    calculate();

    epilog();

    return 0;
}

void calculate( void )
{
    int j;

    N4 = N + 4;

    SET( a, 0 );
    SET( b, 0 );

    for( j = 2 * N4 + 1; j >= 3; j -= 2 )
    {
        SET( c, 1 );
        LONGDIV( c, j );

        SUBTRACT( a, c, a );
        DIVIDE_25( a );

        SUBTRACT( b, c, b );
        DIVIDE_239( b );
        DIVIDE_239( b );

        progress();
    }

    SET( c, 1 );

    SUBTRACT( a, c, a );
    DIVIDE_5( a );

    SUBTRACT( b, c, b );
    DIVIDE_239( b );

    MULTIPLY( a, 4 );
    SUBTRACT( a, a, b );
    MULTIPLY( a, 4 );

    progress();
}

/*

 N = 10000
 A = 0
 B = 0
 J = 2 * (N + 4) + 1
 FOR J = J TO 3 STEP -2
     A = (1 / J - A) / 5 ^ 2
     B = (1 / J - B) / 239 ^ 2
 NEXT J
 A = (1 - A) / 5
 B = (1 - B) / 239
 PI = (A * 4 - B) * 4

*/

void progress( void )
{
    printf(".");
}

void epilog( void )
{
    int j;

    {
        fprintf( stdout, " \n3.");
        for( j = 1; j <= N; j++ )
        {
            fprintf( stdout, "%d", a[j]);
            if( j % 5  == 0 )
                if( j % 50 == 0 )
                    if( j % 250  == 0 )
                        fprintf( stdout, "    <%d>\n\n   ", j );
                    else
                        fprintf( stdout, "\n   " );
                else
                    fprintf( stdout, " " );
        }
    }
}
