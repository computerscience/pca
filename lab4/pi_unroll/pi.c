#include <memory.h>
#include <stdio.h>
#include <stdlib.h>

int N, N4;
char a[10240], b[10240], c[10240];
char string[100];

// 239
// Fer una taula que donat un digit (0..9)
// i un residu (0..238) retorni una q i un r

typedef unsigned char QR;

QR table239q[2390];
QR table239r[2390];
QR table25q[250];
QR table25r[250];
QR qr;

inline unsigned hi(QR n) {
  return (0xFF00 & n) >> 8;
}

inline unsigned lo(QR n) {
  return (0xFF & n);
}

QR table5[10] = {0,1,2,3,4,0,1,2,3,4};

void init_table239(void) {
  int i;
  QR j, k;
  for (j=0; j < 239; ++j) {
    for (i=0, k=0; i < 2390; i+=239, k++){
      table239q[i+j] = k;
      table239r[i+j] = j;
    }
  }
}

void init_table25(void) {
  int i;
  QR j, k;
  for (j=0; j < 25; ++j) {
    for (i=0, k=0; i < 250; i+=25, k++){
      table25q[i+j] = k;
      table25r[i+j] = j;
    }
  }
}

void DIVIDE_239( char *x)
{
   unsigned index, k;
   QR r;
    r = 0;
    for( k = 0; k <= N4; k++ )
    {
      index = ((unsigned) r) * 10 + (unsigned)x[k];
      x[k] = table239q[index];
      r = table239r[index];
    }
}

void DIVIDE_25( char *x)
{
    unsigned index, k;
    QR r;
    r = 0;
    for( k = 0; k <= N4; k++ )
    {
      index = ((unsigned) r) * 10 + (unsigned)x[k];
      x[k] = table25q[index];
      r = table25r[index];
    }
}
void DIVIDE_5( char *x)
{
    int j, k;
    unsigned int q, r, u;
    long v;

    r = 0;
    for( k = 0; k <= N4; k++ )
    {
      //      printf("%d %d\n", r , x[k]);
      q = r*2;
      r = table5[x[k]];
      // Test if x[k] is greater than 4
      q += ((x[k] & 8) >> 3) | ((x[k] & 4) && (x[k] & 3));
      x[k] = q;
    }
}


void LONGDIV( char *x, int n )
{
    int j, k;
    unsigned q, r, u;
    long v;

    if( n < 6553 )
    {
        r = 0;
        for( k = 0; k <= N4; k++ )
        {
            u = r * 10 + x[k];
            q = u / n;
            r = u - q * n;
            x[k] = q;
        }
    }
    else
    {
        r = 0;
        for( k = 0; k <= N4; k++ )
        {
            if( r < 6553 )
            {
                u = r * 10 + x[k];
                q = u / n;
                r = u - q * n;
            }
            else
            {
                v = (long) r * 10 + x[k];
                q = v / n;
                r = v - q * n;
            }
            x[k] = q;
        }
    }
}

void MULTIPLY( char *x, int n )
{
    int j, k;
    unsigned q, r, u;
    long v;
    r = 0;
    for( k = N4; k >= 0; k-- )
    {
        q = n * x[k] + r;
        r = q / 10;
        x[k] = q - r * 10;
    }
}

void SET( char *x, int n )
{
    memset( x, 0, N4 + 1 );
    x[0] = n;
}


void SUBTRACT( char *x, char *y, char *z )
{
    int j, k;
    unsigned q, r, u;
    long v;
    for( k = N4; k >= 0; k-- )
    {
        if( (x[k] = y[k] - z[k]) < 0 )
        {
            x[k] += 10;
            z[k-1]++;
        }
    }
}


void calculate( void );
void progress( void );
void epilog( void );


int main( int argc, char *argv[] )
{
    N = 10000;
    if (argc>1) N = atoi(argv[1]);
    setbuf(stdout, NULL);


    init_table239();
    init_table25();

    calculate();

    epilog();

    return 0;
}

void calculate( void )
{
    int j;

    N4 = N + 4;

    SET( a, 0 );
    SET( b, 0 );

    for( j = 2 * N4 + 1; j >= 3; j -= 2 )
    {
        SET( c, 1 );
        LONGDIV( c, j );

        SUBTRACT( a, c, a );
        DIVIDE_25( a );

        SUBTRACT( b, c, b );
        DIVIDE_239( b );
        DIVIDE_239( b );

        progress();
    }

    SET( c, 1 );

    SUBTRACT( a, c, a );
    DIVIDE_5( a );

    SUBTRACT( b, c, b );
    DIVIDE_239( b );

    MULTIPLY( a, 4 );
    SUBTRACT( a, a, b );
    MULTIPLY( a, 4 );

    progress();
}

/*

 N = 10000
 A = 0
 B = 0
 J = 2 * (N + 4) + 1
 FOR J = J TO 3 STEP -2
     A = (1 / J - A) / 5 ^ 2
     B = (1 / J - B) / 239 ^ 2
 NEXT J
 A = (1 - A) / 5
 B = (1 - B) / 239
 PI = (A * 4 - B) * 4

*/

void progress( void )
{
    printf(".");
}

void epilog( void )
{
    int j;

    {
        fprintf( stdout, " \n3.");
        for( j = 1; j <= N; j++ )
        {
            fprintf( stdout, "%d", a[j]);
            if( j % 5  == 0 )
                if( j % 50 == 0 )
                    if( j % 250  == 0 )
                        fprintf( stdout, "    <%d>\n\n   ", j );
                    else
                        fprintf( stdout, "\n   " );
                else
                    fprintf( stdout, " " );
        }
    }
}
