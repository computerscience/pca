from fast.benchmarks import benchmark, BenchmarkBase

@benchmark
class UnrollingBenchmark(BenchmarkBase):
    target = 'pi.3'
    instances = 5
    executions = 1
    xlabel = "Execution"

    def input(self, instance):
        return ""

    def args(self, instance):
        # To avoid errors when unrolling, make sure
        # the number (+4) has no divisors
        return [10003]
