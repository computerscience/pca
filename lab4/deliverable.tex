\documentclass[a4paper,11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[justification=centering,labelfont=bf]{caption}
\usepackage{graphicx}
\newcommand{\assignatura}{Programació Conscient de l'Arquitectura}
\newcommand{\titol}{Lab4: Flow Control Optimizations}

\newcommand{\Pautor}{Héctor Ramón Jiménez}
\newcommand{\Sautor}{Alvaro Espuña Buxó}

\newcommand{\HRule}{\rule{\linewidth}{0.5mm}}

\renewcommand{\t}[1]{\texttt{#1}}

\begin{document}
\begin{titlepage}
  \begin{center}
    \textsc{\Large \assignatura}\\[1.5cm]
    \HRule \\[0.4cm]
     { \huge \bfseries \titol \\[0.4cm] }
    \HRule \\[2.5cm]
    \begin{minipage}{0.4\textwidth}
      \begin{flushleft}
        \large \Pautor
      \end{flushleft}
    \end{minipage}
    \begin{minipage}{0.4\textwidth}
      \begin{flushright}
        \large \Sautor
      \end{flushright}
    \end{minipage}

    \vfill
    {\large \today}
  \end{center}
\end{titlepage}

\section{Inlining}
  \begin{enumerate}
    \item In this exercise we will analyze the performance impact of
      the \emph{inlining} optimization in the ORIGINAL VERSION of the \t{pi.c}
      program (again given in the \t{lab4\_session.zip} file).
      \begin{enumerate}
        \item Perform timing of the \t{pi.c} compiled with O3
          optimization level and without \emph{inlining} (Do
            \t{make pi.3ni} to compile the \t{pi.c} program with
            \t{-fno-inline} flag, that is, without \emph{inlining}).

        \item Perform timing of the \t{pi.c} compiled with O3 optimization
          level. In that case, the compiler applies \emph{inlining}. Which is
          the speedup compared to the previous execution?

          \begin{figure}[h!]
            \center
            \includegraphics[width=0.72\textwidth]{pi/inlining_time.pdf}
            \caption{Time comparison of \t{pi.c} inlined and (\t{pi.3}) and
              without inlining (\t{pi.3ni})}
          \end{figure}
          \begin{figure}[h!]
            \center
            \includegraphics[width=0.72\textwidth]{pi/inlining_speedup.pdf}
            \caption{Speedup of inlining \t{pi.c}}
          \end{figure}

          \newpage

        \item Indicate which routines are inlined by the compiler.

          {\bf All the routines inside calculate. That is \t{SET}, \t{LONGDIV},
          \t{SUBTRACT}, \t{DIVIDE}, \t{MULTIPLY} and \t{progress}. \t{calculate}
          has only calls to library routines (\t{memset}, \t{putchar}...).}

        \item Analyze if the compiler could do other optimizations
          once it inlined those routines, and enumerate which
          optimizations has been done if that is the case. Hint: Use
          \t{pin} tool and check if there has been any significant change
          on the number of executed instructions of different types of
          instructions as divide, multiply and shift operations.

          {\bf Because of the inlining the compiler can propagate constant values.
          This allows it to, for example, optimize multiplication and divisions
          using shifts.
          % TODO: List all the optimizations
          }

        \item Re-write \t{DIVIDE} as a macro and compile the program with
          O3 optimization level but not \emph{inlining} (compile with
          \t{-fno-inline}). Compare the execution time of this new version
          to the original code, compiled with O3 optimization level.

          {\bf
          Using a \t{define} forces the compiler to \emph{inline} it
          (even with \t{-fno-inline}). \emph{Generic} divisions are so
          expensive, that allowing the compiler to transform them to
          operations with lower latency (because of the constant propagation)
          represents almost all of the speedup.

          Time with \t{DIVIDE} as a \t{define}: 3.12 s}

      \end{enumerate}
  \end{enumerate}

  \newpage
\section{Loop unrolling}
\begin{enumerate}
  \setcounter{enumi}{1}

  \item In this exercise you will analyze the performance impact of
    applying loop \emph{unrolling} to \t{matriu4x4.c} program. This program
    generates two 4x4 matrices and multiply them. In order to avoid
    that compiler makes loop \emph{unrolling} , compile your program with O2
    (\t{make matriu4x4.2} generates the executable compiling with O2
    optimization level).

   \begin{enumerate}
     \item Make \emph{inlining} of the routine \t{multiplica} by hand.
       \begin{enumerate}
         \item Do timing of the program.

           \begin{figure}[ht!]
            \center
            \includegraphics[width=0.72\textwidth]{matriu4x4/multiplica_inlining_time.pdf}
          \end{figure}
          \begin{figure}[ht!]
            \center
            \includegraphics[width=0.72\textwidth]{matriu4x4/multiplica_inlining_speedup.pdf}
            \caption{Effects of inlining \t{multiplica} routine.}
          \end{figure}


        \item Looking at the assembler, compute the approximated
          number of instructions done to multiply two matrices.

          {\bf Approximatelly 1170 instructions per multiplication.}

       \end{enumerate}
       \newpage
     \item Make full \emph{unrolling} of the inner loop of the code (Advice:
       use macros).
       \begin{enumerate}
         \item Do timing of the program. Compute speedup compared to
           previous versions.
           \begin{figure}[ht!]
             \center
             \includegraphics[width=0.72\textwidth]{matriu4x4/unroll_k_time.pdf}
           \end{figure}
           \begin{figure}[ht!]
             \center
             \includegraphics[width=0.72\textwidth]{matriu4x4/unroll_k_speedup.pdf}
             \caption{Effects unrolling the most inner loop.}
           \end{figure}

         \item Looking at the assembler, compute the approximated
           number of instructions done to multiply two matrices.

           {\bf Approximatelly 320 instructions per multiplication.}
       \end{enumerate}
       \newpage
     \item Make full \emph{unrolling} of the next inner loop of the code.
       \begin{enumerate}
       \item Do timing of the program. Compute speedup compared to
         previous versions.

         \begin{figure}[ht!]
             \center
             \includegraphics[width=0.72\textwidth]{matriu4x4/unroll_j_time.pdf}
           \end{figure}
           \begin{figure}[ht!]
             \center
             \includegraphics[width=0.72\textwidth]{matriu4x4/unroll_j_speedup.pdf}
             \caption{Effects of unrolling the two most inner loops}
           \end{figure}

       \item Looking at the assembler, compute the approximated
         number of instructions done to multiply two matrices.

         {\bf Approximatelly 70 instructions per multiplication.}
       \end{enumerate}
       \newpage
     \item Make full \emph{unrolling} of the three loops of the code.
       \begin{enumerate}
       \item Do timing of the program. Compute speedup compared to
         previous versions.
         \begin{figure}[ht!]
           \center
           \includegraphics[width=0.72\textwidth]{matriu4x4/unroll_i_time.pdf}
         \end{figure}
         \begin{figure}[ht!]
           \center
           \includegraphics[width=0.72\textwidth]{matriu4x4/unroll_i_speedup.pdf}
           \caption{Effects of full unrolling.}
         \end{figure}
       \item Looking at the assembler, compute the approximated
         number of instructions done to multiply two matrices.

         {\bf Approximatelly 60 instructions per multiplication.}

       \end{enumerate}
     \item After doing those experiments above, which is the \emph{unrolling}
       degree that gives best performance? Why? Justify your answer
       based on what you have seen in the profiling and assembler code.

       {\bf Fully unrolling all the three loops. A matrix
         multiplication takes less instructions, that have lower
         latency and it has a very low overhead from branching.  Also
         because of the full unrolling we give more freedom to the
         compiler to arrange the operations in whatever order it wants.}

   \end{enumerate}
   \item Apply to the ORIGINAL CODE of \t{matriu4x4.c} (so that, you don’t
     have \emph{inlining} applied to the multiplica routine) the \emph{unrolling}
     degree that gives the best performance in the previous exercise
     using O2 optimization.

     \begin{enumerate}
     \item Compile it using O3 optimization.
       \item Do timing of the program.
         {\bf Time: 0.443 s}
       \item Looking at the assembler, compute the approximated number
         of instructions done to multiply two matrices.
         {\bf Approximately 280 instructions per call to multiplica}
       \item Compute speedup compared to the versions of the previous
         exercises.
         {\bf Speedup over unrolled\_i: 0.339}
       \item Why the version with \emph{inlining} by hand, in the previous
         exercise with O2 optimization, is faster than this version
         without \emph{inlining} by hand and O3 optimization? Note: It is not
         just a fact of the \emph{inlining}.

         In the inlined O2 version, the compiler can do a lot of \emph{preprocessing}.
         The compiler knows what A and B are and how they are going to be used.
         This reduces the matrix multiplication to only \t{addl}ing and \t{movl}ing.
         So in the long run even doing more operations for only one multiplication
         beats O3 not inlined, that has to do \t{imull}s for every iteration.

     \end{enumerate}
\end{enumerate}

\newpage
\section{Optimizations done to \texttt{pi}}

\subsection{Removing unnecessary \texttt{if}s}
\t{LONGDIV} function checked everytime if the number dividing was
greater than 6553. It was doing so, to avoid overflowing in case the
\t{int} is only 16 bits.  Knowing it will run on a machine where both
\t{long}s and \t{int}s have at least 32 bits, we can remove the branching.

\subsection{Avoid branching on \t{SUBTRACT}}

The original code inside the for loop looked like this:
\begin{verbatim}
if( (x[k] = y[k] - z[k]) < 0 )
{
    x[k] += 10;
    z[k-1]++;
}
\end{verbatim}
As we can see we need to do something in case \t{y[k] - z[k]} is negative.
To avoid the branching we can substitute the previous code with this snippet:
\begin{verbatim}
char aux = y[k] - z[k];
x[k] = aux + ((aux >> 7) & 10);
z[k-1] = z[k-1] + ((aux >> 7) & 1);
\end{verbatim}
\t{aux >> 7} becomes 0 if \t{y[k] - z[k]} is positive, and 255
otherwise (because os the sign extension). This way we execute always
the same operations, giving the same result as before, but without the
need of branching.

\subsection{Loop unrolling}
To reduce the loop overhead, we unrolled most of them using the folowing macros:
\begin{verbatim}
#define D239_ITER(_k) {				\
   index = ((unsigned) r) * 10 + (unsigned)x[(_k)];	\
   x[(_k)] = table239q[index];				\
   r = table239r[index]; }

#define D25_ITER(_k) {				\
   index = ((unsigned) r) * 10 + (unsigned)x[(_k)];	\
   x[(_k)] = table25q[index];				\
   r = table25r[index]; }

#define DIV_ITER(_k) {				\
    u = r * 10 + x[(_k)];			\
    q = u / n;					\
    r = u % n; x[(_k)] = q; }

#define MUL_ITER(_k) {				\
    q = n * x[(_k)] + r;			\
    r = q / 10;					\
    x[(_k)] = q - r * 10; }

#define SUB_ITER(_k) {				\
    aux = y[(_k)] - z[(_k)];			\
    x[(_k)] = aux + ((aux >> 7) & 10);		\
    z[(_k)-1] = z[(_k)-1] + ((aux >> 31) & 1); }
\end{verbatim}

Then we can write something like this (for \t{DIVIDE\_239} for
example):
\begin{verbatim}
for( k = 0; k <= N4 - 9; k+=10 )
{
    D239_ITER(k);
    D239_ITER(k+1);
    D239_ITER(k+2);
    D239_ITER(k+3);
    D239_ITER(k+4);
    D239_ITER(k+5);
    D239_ITER(k+6);
    D239_ITER(k+7);
    D239_ITER(k+8);
    D239_ITER(k+9);
}
for(; k <= N4; k++ ) {
    D239_ITER(k);
}
\end{verbatim}
Because of the unrolling some functions (like \t{SUBTRACT}) were not
inlined automatically by the compiler, giving greater execution times.
The solution to this is force the compiler to do inlining likeso:
\begin{verbatim}
__attribute__((always_inline))
inline void SUBTRACT( char *x, char *y, char *z )
\end{verbatim}

\subsection{Buffering}
Depending on the \t{N}, the number of \t{printf}s performed can be huge.
For this reason, and to reduce the performance footprint of the system calls
we use a buffer of 1024 \t{char}s to print the \t{progress} and the result
of the execution.
\end{document}
