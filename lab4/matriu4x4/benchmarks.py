from fast.benchmarks import benchmark, BenchmarkBase


@benchmark
class MultiplicaInliningBenchmark(BenchmarkBase):
    executables = ('matriu4x4.2', 'matriu4x4_inlining.2')
    instances = 10
    executions = 1
    xlabel = "Execution"


@benchmark
class UnrollKBenchmark(MultiplicaInliningBenchmark):
    executables = ('matriu4x4.2', 'matriu4x4_unroll_k.2')


@benchmark
class UnrollJBenchmark(MultiplicaInliningBenchmark):
    executables = ('matriu4x4.2', 'matriu4x4_unroll_j.2')


@benchmark
class UnrollIBenchmark(MultiplicaInliningBenchmark):
    executables = ('matriu4x4.2', 'matriu4x4_unroll_i.2')
