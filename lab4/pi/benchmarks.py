from fast.benchmarks import benchmark, BenchmarkBase

@benchmark
class InliningBenchmark(BenchmarkBase):
    executables = ('pi.3ni', 'pi.3')
    instances = 10
    executions = 1
    xlabel = "Execution"

    def input(self, instance):
        return ''
