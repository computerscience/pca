from fast.benchmarks import benchmark, BenchmarkBase

@benchmark
class ProteinBenchmark(BenchmarkBase):
    target = '3d_original'
    candidates = ('3d_optimized',)
    instances = 3
    executions = 1
    xlabel = "Test"
    diff_script = "diff.sh"

    statics = ["2pka", "1hba", "4hhb"]

    def input(self, instance):
        return ""

    def args(self, instance):
        return [
            "-static", "../inputs/%s.parsed" % self.statics[instance-1],
            "-mobile", "../inputs/5pti.parsed"
        ]
