#!/usr/bin/env bash

EXECUTABLE=optimized/progs/ftdock
FORMAT="      %e (%Uu %Ss) cpu: %P mem: %Mk faults: %FM+%Rm"

color() {
    color=$1
    text=$2
    echo -e "\e[38;05;${color}m${text}\e[0m";
}

ok_or_failed() {
  if [ $1 -eq 0 ]; then
    color 2 "ok"
  else
    color 1 "failed"
    exit 1
  fi
}

unittest () {
    outfile=$1
    shift

    echo -n "      Testing ${outfile%.output}..."
    /usr/bin/time -f "$FORMAT" $EXECUTABLE $@ > /tmp/out 2> /tmp/time

    python diff.py /tmp/out $outfile
    ok_or_failed $?

    cat /tmp/time
}


echo -n "    Compiling..."

pushd "optimized/progs/" > /dev/null
make -B -f Makefile > /dev/null
ok_or_failed $?
popd > /dev/null

echo "---------------------"

echo "    Checking differences..."

unittest "../outputs/test1.output" -static ../inputs/2pka.parsed -mobile ../inputs/5pti.parsed
unittest "../outputs/test2.output" -static ../inputs/1hba.parsed -mobile ../inputs/5pti.parsed
unittest "../outputs/test3.output" -static ../inputs/4hhb.parsed -mobile ../inputs/5pti.parsed

echo "    No differences detected!"
echo "---------------------"
