/*
This file is part of ftdock, a program for rigid-body protein-protein docking 
Copyright (C) 1997-2000 Gidon Moont

Biomolecular Modelling Laboratory
Imperial Cancer Research Fund
44 Lincoln's Inn Fields
London WC2A 3PX

+44 (0)20 7269 3348
http://www.bmm.icnet.uk/

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "structures.h"
#include <xmmintrin.h>
#include <pmmintrin.h>
#include <smmintrin.h>

void assign_charges( struct Structure This_Structure ) {

/************/

  /* Counters */

  int	residue , atom ;

/************/

  for( residue = 1 ; residue <= This_Structure.length ; residue ++ ) {
    for( atom = 1 ; atom <= This_Structure.Residue[residue].size ; atom ++ ) {

      This_Structure.Residue[residue].Atom[atom].charge = 0.0 ;

      /* peptide backbone */

      if( strcmp( This_Structure.Residue[residue].Atom[atom].atom_name , " N  " ) == 0 ) {
        if( strcmp( This_Structure.Residue[residue].res_name , "PRO" ) == 0 ) {
          This_Structure.Residue[residue].Atom[atom].charge = -0.10 ;
        } else {
          This_Structure.Residue[residue].Atom[atom].charge =  0.55 ;
          if( residue == 1 ) This_Structure.Residue[residue].Atom[atom].charge = 1.00 ;
        }
      }

      if( strcmp( This_Structure.Residue[residue].Atom[atom].atom_name , " O  " ) == 0 ) {
        This_Structure.Residue[residue].Atom[atom].charge = -0.55 ;
        if( residue == This_Structure.length  ) This_Structure.Residue[residue].Atom[atom].charge = -1.00 ;
      }

      /* charged residues */

      if( ( strcmp( This_Structure.Residue[residue].res_name , "ARG" ) == 0 ) && ( strncmp( This_Structure.Residue[residue].Atom[atom].atom_name , " NH" , 3 ) == 0 ) ) This_Structure.Residue[residue].Atom[atom].charge =  0.50 ;
      if( ( strcmp( This_Structure.Residue[residue].res_name , "ASP" ) == 0 ) && ( strncmp( This_Structure.Residue[residue].Atom[atom].atom_name , " OD" , 3 ) == 0 ) ) This_Structure.Residue[residue].Atom[atom].charge = -0.50 ;
      if( ( strcmp( This_Structure.Residue[residue].res_name , "GLU" ) == 0 ) && ( strncmp( This_Structure.Residue[residue].Atom[atom].atom_name , " OE" , 3 ) == 0 ) ) This_Structure.Residue[residue].Atom[atom].charge = -0.50 ;
      if( ( strcmp( This_Structure.Residue[residue].res_name , "LYS" ) == 0 ) && ( strcmp( This_Structure.Residue[residue].Atom[atom].atom_name , " NZ " ) == 0 ) ) This_Structure.Residue[residue].Atom[atom].charge =  1.00 ;

    }
  }

/************/

}



/************************/



void electric_field( struct Structure This_Structure , float grid_span , int grid_size ,fftw_real *grid ) {

/************/

  /* Counters */

  int residue , atom ;

  /* Co-ordinates */

  int x , y , z ;
  float   x_centre , y_centre , z_centre ;

  /* Variables */

  float   distance ;
  float   phi , epsilon ;

/************/

  for( x = 0 ; x < grid_size ; x ++ ) {
    for( y = 0 ; y < grid_size ; y ++ ) {
      for( z = 0 ; z < grid_size ; z ++ ) {

        grid[gaddress(x,y,z,grid_size)] = (fftw_real)0 ;

      }
    }
  }

/************/

  setvbuf( stdout , (char *)NULL , _IONBF , 0 ) ;

  /************/
  int atom_count = 0;
  for( residue = 1 ; residue <= This_Structure.length ; residue ++ )
    for( atom = 1 ; atom <= This_Structure.Residue[residue].size ; atom ++ )
      atom_count += (This_Structure.Residue[residue].Atom[atom].charge != 0);
    

  // Move this to a global location?
  // If it results useful in some other function, of course!
  float *coords;
  float *charges;
  
  posix_memalign((void**)&coords, 16, atom_count*4 * sizeof(float));
  posix_memalign((void**)&charges, 16, atom_count * sizeof(float));
  
  
  atom_count = 0;
  for( residue = 1 ; residue <= This_Structure.length ; residue ++ ) {
    for( atom = 1 ; atom <= This_Structure.Residue[residue].size ; atom ++ ) {
      if( This_Structure.Residue[residue].Atom[atom].charge != 0 ) {
        coords[atom_count*4]   = This_Structure.Residue[residue].Atom[atom].coord[1];
        coords[atom_count*4+1] = This_Structure.Residue[residue].Atom[atom].coord[2];
        coords[atom_count*4+2] = This_Structure.Residue[residue].Atom[atom].coord[3];
        coords[atom_count*4+3] = 0;
        charges[atom_count] = This_Structure.Residue[residue].Atom[atom].charge;
        
        atom_count++;
      }
    }
  }


  /************/
  
  printf( "  electric field calculations ( one dot / grid sheet ) " ) ;

  __m128 dists, epsis;
  __m128 aux1, aux2, aux3, aux4;
  
  for( x = 0 ; x < grid_size ; x ++ ) {

    printf( "." ) ;

    x_centre  = gcentre( x , grid_span , grid_size ) ;

    for( y = 0 ; y < grid_size ; y ++ ) {

      y_centre  = gcentre( y , grid_span , grid_size ) ;

      for( z = 0 ; z < grid_size ; z ++ ) {

        z_centre  = gcentre( z , grid_span , grid_size ) ;

        phi = 0 ;
        
        __m128 center = _mm_set_ps(0, z_centre, y_centre, x_centre);
        __m128 accum = _mm_setzero_ps();
        
        for(atom = 0; atom < atom_count - 3; atom+=4) {
            // Extended instead of creating a function to exploit throughput and minimize dependencies
            // between operations
            
            aux1 = _mm_load_ps(&coords[atom*4]);
            aux2 = _mm_load_ps(&coords[atom*4 + 4]);
            aux3 = _mm_load_ps(&coords[atom*4 + 8]);
            aux4 = _mm_load_ps(&coords[atom*4 + 12]);
            
            aux1 = _mm_sub_ps(aux1, center);
            aux2 = _mm_sub_ps(aux2, center);
            aux3 = _mm_sub_ps(aux3, center);
            aux4 = _mm_sub_ps(aux4, center);


            aux1 = _mm_mul_ps(aux1, aux1);
            aux2 = _mm_mul_ps(aux2, aux2);
            aux3 = _mm_mul_ps(aux3, aux3);
            aux4 = _mm_mul_ps(aux4, aux4);

            // horizontally add aux1 = [aux1[0]+aux1[1], aux1[2]+aux1[3], aux1[0]+aux1[1], aux1[2]+aux1[3]]
            aux1 = _mm_hadd_ps(aux1, aux1); 
            aux2 = _mm_hadd_ps(aux2, aux2);
            aux3 = _mm_hadd_ps(aux3, aux3);
            aux4 = _mm_hadd_ps(aux4, aux4);

            // aux1 = [aux2[0]+aux2[1], aux2[0]+aux2[1], aux1[0]+aux1[1], aux1[0]+aux1[1]]
            aux1 = _mm_hadd_ps(aux2, aux1);
            aux2 = _mm_hadd_ps(aux4, aux3);
            
            // Now we need to suffle: get 2:0 from each (for example)
            // MASK: 2020: each digit 2 bits => 0x88
            aux1 = _mm_shuffle_ps(aux1, aux2, 0x22);
            
            // Perform 4 sqrt in parallel
            dists = _mm_sqrt_ps(aux1); 
            
            // distance = pythagoras(
            //    coords[atom*4],
            //    coords[atom*4+1],
            //    coords[atom*4+2], 
            //    x_centre , y_centre , z_centre
            //  ) ;
          
          aux2 = _mm_set1_ps(2.0);
          aux1 = _mm_cmplt_ps(dists, aux2);
          dists = _mm_add_ps(_mm_and_ps(aux1, aux2), _mm_andnot_ps(aux1, dists));
         
          //if( distance < 2.0 )
          //  distance = 2.0 ;
          
          aux2 = _mm_set1_ps(8.0);
          aux1 = _mm_cmpge_ps(dists, aux2);
          epsis = _mm_and_ps(_mm_set1_ps(80.0), aux1);
          
          //if( distance >= 8.0 )
          //  epsilon = 80 ;
          
          aux3 = _mm_set1_ps(6.0);
          aux2 = _mm_cmple_ps(dists, aux3);
          epsis = _mm_add_ps(epsis, _mm_and_ps(_mm_set1_ps(4.0), aux2));
          
          //else if( distance <= 6.0 ) 
          //  epsilon = 4 ;
        
          aux4 = _mm_or_ps(aux1, aux2);
          aux3 = _mm_andnot_ps(aux4, dists); // distance if 6.0 < distance < 8.0, 0 otherwise
          epsis = _mm_add_ps(epsis, _mm_sub_ps(_mm_mul_ps(_mm_set1_ps(38.0), aux3), _mm_andnot_ps(aux4, _mm_set1_ps(224.0))));
          
          //else
          //  epsilon = ( 38 * distance ) - 224 ;
        
          accum = _mm_add_ps(_mm_div_ps(_mm_load_ps(&charges[atom]), _mm_mul_ps(epsis, dists)), accum);
          // phi += ( charges[atom] / ( epsilon * distance ) ) ;
        
        }
        
        aux1 = _mm_hadd_ps(accum, accum);
        aux1 = _mm_hadd_ps(aux1, aux1);
        phi = _mm_cvtss_f32(aux1);

        
        // Epilog
        for(; atom < atom_count; atom++) {
          distance = pythagoras(
            coords[atom*4],
            coords[atom*4+1],
            coords[atom*4+2], 
            x_centre , y_centre , z_centre
          ) ;
              
          // (ax - cx)^2 + (ay - cy)^2 + (az - cz)^2
         
          if( distance < 2.0 )
            distance = 2.0 ;

          if( distance >= 8.0 )
            epsilon = 80 ;

          else if( distance <= 6.0 ) 
            epsilon = 4 ;
              
          else
            epsilon = ( 38 * distance ) - 224 ;
  
          phi += ( charges[atom] / ( epsilon * distance ) ) ;
        }
        
        grid[gaddress(x,y,z,grid_size)] = (fftw_real)phi ;
      }
    }
  }

  printf( "\n" ) ;

/************/

  return ;

}



/************************/



void electric_point_charge( struct Structure This_Structure , float grid_span , int grid_size , fftw_real *grid ) {

/************/

  /* Counters */

  int	residue , atom ;

  /* Co-ordinates */

  int	x , y , z ;
  int	x_low , x_high , y_low , y_high , z_low , z_high ;

  float		a , b , c ;
  float		x_corner , y_corner , z_corner ;
  float		w ;

  /* Variables */

  float		one_span ;

/************/

  for( x = 0 ; x < grid_size ; x ++ ) {
    for( y = 0 ; y < grid_size ; y ++ ) {
      for( z = 0 ; z < grid_size ; z ++ ) {

        grid[gaddress(x,y,z,grid_size)] = (fftw_real)0 ;

      }
    }
  }

/************/

  one_span = grid_span / (float)grid_size ;

  for( residue = 1 ; residue <= This_Structure.length ; residue ++ ) {
    for( atom = 1 ; atom <= This_Structure.Residue[residue].size ; atom ++ ) {

      if( This_Structure.Residue[residue].Atom[atom].charge != 0 ) {

        x_low = gord( This_Structure.Residue[residue].Atom[atom].coord[1] - ( one_span / 2 ) , grid_span , grid_size ) ;
        y_low = gord( This_Structure.Residue[residue].Atom[atom].coord[2] - ( one_span / 2 ) , grid_span , grid_size ) ;
        z_low = gord( This_Structure.Residue[residue].Atom[atom].coord[3] - ( one_span / 2 ) , grid_span , grid_size ) ;

        x_high = x_low + 1 ;
        y_high = y_low + 1 ;
        z_high = z_low + 1 ;

        a = This_Structure.Residue[residue].Atom[atom].coord[1] - gcentre( x_low , grid_span , grid_size ) - ( one_span / 2 ) ;
        b = This_Structure.Residue[residue].Atom[atom].coord[2] - gcentre( y_low , grid_span , grid_size ) - ( one_span / 2 ) ;
        c = This_Structure.Residue[residue].Atom[atom].coord[3] - gcentre( z_low , grid_span , grid_size ) - ( one_span / 2 ) ;

        for( x = x_low ; x <= x_high  ; x ++ ) {
 
          x_corner = one_span * ( (float)( x - x_high ) + .5 ) ;

          for( y = y_low ; y <= y_high  ; y ++ ) {

            y_corner = one_span * ( (float)( y - y_high ) + .5 ) ;

            for( z = z_low ; z <= z_high  ; z ++ ) {

              z_corner = one_span * ( (float)( z - z_high ) + .5 ) ;

              w = ( ( x_corner + a ) * ( y_corner + b ) * ( z_corner + c ) ) / ( 8.0 * x_corner * y_corner * z_corner ) ;

              grid[gaddress(x,y,z,grid_size)] += (fftw_real)( w * This_Structure.Residue[residue].Atom[atom].charge ) ;

            }
          }
        }

      }

    }
  }

/************/

  return ;

}



/************************/



void electric_field_zero_core( int grid_size , fftw_real *elec_grid , fftw_real *surface_grid , float internal_value ) {

/************/

  /* Co-ordinates */

  int	x , y , z ;

/************/

  for( x = 0 ; x < grid_size ; x ++ ) {
    for( y = 0 ; y < grid_size ; y ++ ) {
      for( z = 0 ; z < grid_size ; z ++ ) {

        if( surface_grid[gaddress(x,y,z,grid_size)] == (fftw_real)internal_value ) elec_grid[gaddress(x,y,z,grid_size)] = (fftw_real)0 ;

      }
    }
  }

/************/

  return ;

}
