import sys

def read_coords(filename):
    with open(filename) as f:
        data_lines = f.readlines()[-4:-1]
    return {tuple(line.split()[4:7]) for line in data_lines}

tcoords = read_coords(sys.argv[1])
ccoords = read_coords(sys.argv[2])

if not tcoords == ccoords:
    print >>sys.stderr, "---Output mismtach----"
    print >>sys.stderr, sys.argv[1], "\t", tcoords
    print >>sys.stderr, sys.argv[2], "\t", ccoords
    exit(1)
