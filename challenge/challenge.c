#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <emmintrin.h>

#define N 1000000
#define BUFSIZE 1024

// http://en.wikipedia.org/wiki/Tree_of_primitive_Pythagorean_triples
static const char A[3][3] = {{ 1, -2, 2}, { 2, -1, 2}, { 2, -2, 3}};
static const char B[3][3] = {{ 1,  2, 2}, { 2,  1, 2}, { 2,  2, 3}};
static const char C[3][3] = {{-1,  2, 2}, {-2,  1, 2}, {-2,  2, 3}};

static const char DIGITS[] = "0123456789";

char buffer[BUFSIZE];
int16_t buf_ptr = -1;

uint32_t stack[1000 * 3]; // 3 ints per level and max depth 999

// Don't care about overflows, use LIMs to fix them
// This way we reduce memory usage of TRIPLES by 2
uint16_t TRIPLES[N];
static const uint32_t LIM_16bit = 411757;
static const uint32_t LIM_17bit = 823553;

static uint8_t n_digits(uint32_t n) {
  // Should only be called with numbers lesser than a 1000000
  return
    (n >= 100000) ? 6 :
    (n >=  10000) ? 5 :
    (n >=   1000) ? 4 :
    (n >=    100) ? 3 :
    (n >=     10) ? 2 : 1;
}

static void flush(void) {
  write(1, buffer, buf_ptr + 1);
  buf_ptr = -1;
}

static void bufferize(uint32_t n) {
  uint16_t i;

  if (BUFSIZE < buf_ptr + 8) flush();

  // Insert into the buffer the number n as a string and a newline
  if (n == 0) buffer[++buf_ptr] = DIGITS[0];
  else {
      buf_ptr += n_digits(n);
      for(i = buf_ptr; n; --i) {
	buffer[i] = DIGITS[n % 10];
	n /= 10;
      }
  }
  buffer[++buf_ptr] = '\n';
}

static uint32_t read_number() {
  uint32_t n;
  int res;
  res = scanf("%u", &n);
  return res != EOF ? n : 0;
}

static void find_triples(int n) {
  int x, y, z, nz, opt;
  int stack_ptr = 0;

  stack[stack_ptr++] = 3;
  stack[stack_ptr++] = 4;
  stack[stack_ptr++] = 5;

  while (stack_ptr) {
    z = stack[--stack_ptr];
    y = stack[--stack_ptr];
    x = stack[--stack_ptr];

    // TODO Avoid the z "randomness"
    TRIPLES[z - 1]++;

    nz = A[2][0] * x + A[2][1] * y + A[2][2] * z;
    opt = nz <= N;
    stack[stack_ptr] = A[0][0] * x + A[0][1] * y + A[0][2] * z;
    stack[stack_ptr + opt] = A[1][0] * x + A[1][1] * y + A[1][2] * z;
    stack[stack_ptr + opt * 2] = nz;
    stack_ptr += 3 & -opt;

    nz = B[2][0] * x + B[2][1] * y + B[2][2] * z;
    opt = nz <= N;
    stack[stack_ptr] = B[0][0] * x + B[0][1] * y + B[0][2] * z;
    stack[stack_ptr + opt] = B[1][0] * x + B[1][1] * y + B[1][2] * z;
    stack[stack_ptr + opt * 2] = nz;
    stack_ptr += 3 & -opt;

    nz = C[2][0] * x + C[2][1] * y + C[2][2] * z;
    opt = nz <= N;
    stack[stack_ptr] = C[0][0] * x + C[0][1] * y + C[0][2] * z;
    stack[stack_ptr + opt] = C[1][0] * x + C[1][1] * y + C[1][2] * z;
    stack[stack_ptr + opt * 2] = nz;
    stack_ptr += 3 & -opt;
  }
}

static void populate(void) {
  int i;
  __m128i prefix_sum;
  for (i = 1; i < N-20; i+=21) {
    prefix_sum = _mm_loadu_si128((__m128i*) &TRIPLES[i - 1]);
    prefix_sum = _mm_add_epi16(prefix_sum, _mm_slli_si128(prefix_sum, 2));
    prefix_sum = _mm_add_epi16(prefix_sum, _mm_slli_si128(prefix_sum, 4));
    prefix_sum = _mm_add_epi16(prefix_sum, _mm_slli_si128(prefix_sum, 8));
    _mm_storeu_si128((__m128i*) &TRIPLES[i - 1], prefix_sum);
    prefix_sum = _mm_loadu_si128((__m128i*) &TRIPLES[i + 6]);
    prefix_sum = _mm_add_epi16(prefix_sum, _mm_slli_si128(prefix_sum, 2));
    prefix_sum = _mm_add_epi16(prefix_sum, _mm_slli_si128(prefix_sum, 4));
    prefix_sum = _mm_add_epi16(prefix_sum, _mm_slli_si128(prefix_sum, 8));
    _mm_storeu_si128((__m128i*) &TRIPLES[i + 6], prefix_sum);
    prefix_sum = _mm_loadu_si128((__m128i*) &TRIPLES[i + 13]);
    prefix_sum = _mm_add_epi16(prefix_sum, _mm_slli_si128(prefix_sum, 2));
    prefix_sum = _mm_add_epi16(prefix_sum, _mm_slli_si128(prefix_sum, 4));
    prefix_sum = _mm_add_epi16(prefix_sum, _mm_slli_si128(prefix_sum, 8));
    _mm_storeu_si128((__m128i*) &TRIPLES[i + 13], prefix_sum);

  }
  for (; i < N; i++)
    TRIPLES[i] += TRIPLES[i - 1];
}

int main()
{
  uint32_t n;
  find_triples(N);

  populate();

  while ((n = read_number())) {
    bufferize(((n >= LIM_17bit) << 16) + ((n >= LIM_16bit) << 16) + TRIPLES[n - 1]);
  }
  flush();
  return 0;
}
