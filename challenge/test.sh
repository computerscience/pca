#!/usr/bin/env bash

color() {
    color=$1
    text=$2
    echo -e "\e[38;05;${color}m${text}\e[0m";
}

echo -n "Compiling..."
out=$(make &1> /dev/null)
[ $? -eq 0 ] && color 2 "done" || { color 1 "error" && echo "$out" && exit 1; }

unittest () {
    infile=$1
    outfile=${infile%.i}.o

    echo -n "Testing ${infile%.i}..."
    t=$(/usr/bin/time -f "u%U s%S e%E %M w%O %F+%R" 2>&1 \
	./challenge.3 < ${infile} > /tmp/out)
    out=$(diff -u ${outfile} /tmp/out)
    [ $? -eq 0 ] && { color 2 "ok" && color 3 "${t}"; } || \
	{ color 1 "failed" && echo "$out" && exit 1; }
}


for file in *.i
do
    unittest $file
done
