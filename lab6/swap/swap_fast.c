#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <stdint.h>
#include <emmintrin.h>

#define BUFFER_SIZE (16*1024)

#define ITER128  *ptr.as_128 =						\
    _mm_or_si128(							\
		 _mm_slli_epi16(*ptr.as_128, 8),			\
		 _mm_srli_epi16(*ptr.as_128, 8)				\
									); \
  ptr.as_128++;

#define ITER16  *ptr.as_16 = ((*ptr.as_16 << 8) & 0xFF00) |		\
                             ((*ptr.as_16 >> 8) & 0x00FF);	\
  ptr.as_16++;


int main(int argc, char *argv[]) {
    union {
        char* as_8;
        uint16_t* as_16;
	__m128i* as_128;
    } ptr;
    __m128i mask1 = _mm_set1_epi16(0xFF00);
    __m128i mask2 = _mm_set1_epi16(0x00FF);

    char buffer[BUFFER_SIZE] __attribute__((aligned (16)));
    char *end;
    char temp;
    int n;

    while ((n=read(0, &buffer, BUFFER_SIZE)) > 0) {
        ptr.as_8 = buffer;
        end = ptr.as_8 + n;

        while(ptr.as_8 < (end - (16*4 - 1))) {
	    ITER128;
	    ITER128;
	    ITER128;
	    ITER128;
        }

        while(ptr.as_8 < (end - 1)) {
	    ITER16;
        }

        fwrite(buffer, sizeof(char), n, stdout);

    }

    return 0;
}
