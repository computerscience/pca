from fast.benchmarks import benchmark, BenchmarkBase

@benchmark
class BrightnessBenchmark(BenchmarkBase):
    target = 'increase_brightness.3'
    instances = 5
    executions = 1
    xlabel = "Execution"

    def input(self, instance):
        return ""

    def args(self, instance):
        if instance == 1:
            return []
        return [1,  100000 * instance]
