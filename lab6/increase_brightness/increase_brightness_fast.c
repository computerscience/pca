#include <stdio.h>
#include <stdlib.h>
#include <emmintrin.h>

#define N 16000000
#define STRIDE 4*16

typedef struct s_pixel_rgb {
	unsigned char r;
	unsigned char g;
	unsigned char b;
} t_pixel_rgb;

#define saturate(x) ((x) + inc) > 255 ? 255: ((x) + inc);

void increase_brightness (t_pixel_rgb *rgb, int len, unsigned char inc, int N_iter)
{
    int i,j;
    __m128i iinc = _mm_set1_epi8(inc);
    char *lim = (char *)(rgb + len);

    for (j=0; j<N_iter; j++) {
	__m128i *k = (__m128i *) rgb;
	char *p = (char *) rgb;
	for (; p < (lim - (STRIDE - 1)); p += STRIDE) {
	    *k = _mm_adds_epu8(*k, iinc);
	    k++;
	    *k = _mm_adds_epu8(*k, iinc);
	    k++;
	    *k = _mm_adds_epu8(*k, iinc);
	    k++;
	    *k = _mm_adds_epu8(*k, iinc);
	    k++;
	}
	for (; p < lim; p++)
	    *p = saturate(*p);
    }
}

int main (int argc, char *argv[])
{

  t_pixel_rgb *A;
  int i;
  int n=N; // tamanyo vectores
  int NIt=50; // Num.iters
  unsigned char incr=1;

  if (argc>1) NIt = atoi(argv[1]);
  if (argc>2) n = atoi(argv[2]);
  if (argc>3) incr = (unsigned char) atoi(argv[3]);

  if (n>N) { printf("Maxima longitud vector: %d\n",N); exit(-1); }

  if (posix_memalign((void**)&A, 16, n*sizeof(t_pixel_rgb)) != 0) {
      fprintf(stderr, "No hay memoria.\n");
      exit(-1);
  }

   /* Inicialitzacio nombres "aleatoris" */
   for (i=0;i<n;i++) {
     A[i].r= i^(i-54) & 0x2f;
     A[i].g= (i+89)^(i-200) & 0x2f;
     A[i].b= (i+70)^(i+325) &0x2f;
   }

   increase_brightness (A, n, incr, NIt);

   write(1, A, n*sizeof(t_pixel_rgb));

   return(0);
}
