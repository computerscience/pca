from fast.benchmarks import benchmark, BenchmarkBase


def make_benchmark(bits, opt):
    return benchmark(
        type(
            "O%d_%d" % (opt, bits),
            (BenchmarkBase,),
            {
                'instances': 5,
                'executions': 1,
                'xlabel': 'Execution',
                'target': 'vector_add.e%d.%d' % (bits, opt),
                'candidates': ['vector_add.v%d.%d' % (bits, opt)],
                'input': lambda self, ins: ""
            }
        )
    )

o2_8 = make_benchmark(8, 2)
o2_16 = make_benchmark(16, 2)
o2_32 = make_benchmark(32, 2)

o3_8 = make_benchmark(8, 3)
o3_16 = make_benchmark(16, 3)
o3_32 = make_benchmark(32, 3)
