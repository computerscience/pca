from fast.benchmarks import benchmark, BenchmarkBase


@benchmark
class RGB2YUVBenchmark(BenchmarkBase):
    target = 'rgb2yuv_imprecise.pca'
    candidates = ['rgb2yuv_fast.pca']
    instances = 5
    executions = 1
    xlabel = "Execution"
