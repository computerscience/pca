#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <emmintrin.h>

#define N 4000000
#define S 256

#define ITER(index) { \
    yuv[(index)].y =   0.299*rgb[(index)].r + 0.587*rgb[(index)].g + 0.114*rgb[(index)].b; \
    yuv[(index)].u = - 0.147*rgb[(index)].r - 0.289*rgb[(index)].g + 0.436*rgb[(index)].b; \
    yuv[(index)].v =   0.615*rgb[(index)].r - 0.515*rgb[(index)].g - 0.100*rgb[(index)].b; \
};

typedef struct s_pixel_rgb {
	float r;
	float g;
	float b;
} t_pixel_rgb;

typedef struct s_pixel_yuv {
	float y;
	float u;
	float v;
} t_pixel_yuv;

void rgb_to_yuv(t_pixel_rgb *rgb, t_pixel_yuv *yuv, int len, int N_iter)
{
  int i,j,ii;

    float *ys, *us, *vs;

    posix_memalign((void**)&ys, 16, S*sizeof(float));
    posix_memalign((void**)&us, 16, S*sizeof(float));
    posix_memalign((void**)&vs, 16, S*sizeof(float));

    for (j=0; j<N_iter; j++) {
      for (i = 0; i < len; i += S) {
	for (ii = i; ii < len && ii < i + S; ii++)
	  ys[ii%256] = 0.299*rgb[ii].r + 0.587*rgb[ii].g + 0.114*rgb[ii].b;

	for (ii = i; ii < len && ii < i + S; ii++)
	  us[ii%256] = -0.147*rgb[ii].r - 0.289*rgb[ii].g + 0.436*rgb[ii].b;

	for (ii = i; ii < len && ii < i + S; ii++)
	  vs[ii%256] = 0.615*rgb[ii].r - 0.515*rgb[ii].g - 0.100*rgb[ii].b;

	for (ii = i; ii < len && ii < i + S; ii++) {
	  yuv[ii].y = ys[ii%256];
	  yuv[ii].u = us[ii%256];
	  yuv[ii].v = vs[ii%256];
	}
      }
    }
}

int main (int argc, char *argv[])
{
  t_pixel_rgb *rgb; t_pixel_yuv *yuv; // Vectores para malloc

  int i;
  int n=N; // tamanyo vectores
  int NIt=50; // Num.iters

  if (argc>1) NIt = atoi(argv[1]);
  if (argc>2) n = atoi(argv[2]);

  if (n>N) { printf("Maxima longitud vector: %d\n",N); exit(-1); }

  if (posix_memalign((void**)&rgb, 16, n*sizeof(t_pixel_rgb)) !=0) {
      fprintf(stderr, "No memory.\n");
      exit(-1);
  }
  if (posix_memalign((void**)&yuv, 16, n*sizeof(t_pixel_yuv)) !=0) {
      fprintf(stderr, "No memory.\n");
      exit(-1);
  }

  for (i=0;i<n;i++) {
     rgb[i].r=(float) ((i<<26) ^ ((i + 340) << 22) ^ i ^ 0xf215fabc);
     rgb[i].g=(float) (((i+450)<<27) ^ ((i + 567) <<20) ^ i ^ 0xb152e8ca);
     rgb[i].b=(float) (((i+7823454) << 25) ^0xbad51cde);
  }

  rgb_to_yuv(rgb, yuv, n, NIt);

  write(1, yuv, n*sizeof(t_pixel_yuv));

  return(0);
}
