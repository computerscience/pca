#include <math.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

#define N 6000
#define PUNTS 1000
#define BUFFSIZE 512

double sines[PUNTS];
double coses[PUNTS];

double buffer[BUFFSIZE];

int main(int argc, char *argv[])
{
  unsigned int i, r, j, n, bp=0;
        double d, x, y;

        if (argc == 1) n = N; else n = atoi(argv[1]);

        srand(0);

	r = rand();
        for (j=0, d=0; j<PUNTS; j++)
          {
            coses[j] = cos(d);
            sines[j] = sin(d);
            x = r*coses[j]; y = r*sines[j];
            d += 2*M_PI/PUNTS;
	    buffer[bp] = x;
	    ++bp;
	    buffer[bp] = y;
	    ++bp;
	    if (bp == BUFFSIZE) {
	      fwrite(buffer, sizeof(double), BUFFSIZE, stdout);
	      bp = 0;
	    }
         }


        for (i=1; i<n; i++)
        {
                r = rand();
                for (j=0, d=0; j<PUNTS; j++)
                {

                        x = r*coses[j]; y = r*sines[j];
                        d += 2*M_PI/PUNTS;
			buffer[bp] = x;
			++bp;
			buffer[bp] = y;
			++bp;
			if (bp == BUFFSIZE) {
			  fwrite(buffer, sizeof(double), BUFFSIZE, stdout);
			  bp = 0;
			}
                }
        }
	fwrite(buffer, sizeof(double), bp, stdout);
        return 0;
}
